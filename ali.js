class AliExpress extends ShopPurchase {
  constructor({
    selectClass,
    countClass,

    parentNodeLevelToSelect,
    parentNodeLevelToCount,

    selectorTitle,
    selectorCount,
    selectorPrice,
    selectorImage
  }) {
    super()

    this.selectClass = selectClass
    this.countClass = countClass

    this.parentNodeLevelToSelect = parentNodeLevelToSelect
    this.parentNodeLevelToCount = parentNodeLevelToCount

    this.selectorTitle = selectorTitle
    this.selectorCount = selectorCount
    this.selectorPrice = selectorPrice
    this.selectorImage = selectorImage

    this.purchases = []
    this.totalPrice = 0
  }

  get allPurchases() {
    return this.purchases
  }

  get totalSum() {
    return this.totalPrice
  }

  #getProductInfoFromNode = (node, level) => {
    const productNode = super.getNodeFromLevel(node, level)

    return {
      title: productNode.querySelector(this.selectorTitle).innerHTML,
      price: productNode.querySelector(this.selectorPrice).innerHTML,
      count: productNode.querySelector(this.selectorCount).value,
      image: productNode.querySelector(this.selectorImage).src
    }
  }

  calculatePrice() {
    this.totalPrice = super.calculatePrice(this.purchases)
    console.log(this.totalPrice)
  }

  selectAliExpressProduct(node) {
    const product = this.#getProductInfoFromNode(node, this.parentNodeLevelToSelect)

    if (!this.purchases.find(item => item.image === product.image)) {
      this.purchases.push(product)
    } else {
      this.purchases = this.purchases.filter(item => item.image !== product.image)
    }
    this.calculatePrice()
  }

  updatePurchaseCount(node) {
    const product = this.#getProductInfoFromNode(node, this.parentNodeLevelToCount)

    this.purchases = this.purchases.map(item => item.image === product.image ? product : item)
    this.calculatePrice()
  }
}
