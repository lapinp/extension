window.onload = function() {
  const domCreator = new DomCreator()

  const ali = new AliExpress({
    // click classes
    selectClass: 'next-checkbox-input',
    countClass: 'next-icon',

    // deep values between parent block / event targets
    parentNodeLevelToSelect: 4,
    parentNodeLevelToCount: 8,

    // selectors for get node info
    selectorTitle: '.product-name-link',
    selectorCount: '.product-num input',
    selectorPrice: '.main-cost-price',
    selectorImage: '.product-image img'
  })

  const currys = new Currys({
    // selector for actions
    productSelector: '[data-element="ProductInfo"]',
    changeCountSelector: '[data-element="ListItem"]',
    removeSelector: '[data-component="RemoveButton"]',

    // deep values between parent block / event targets
    parentNodeLevelToRemove: 5,
    parentNodeLevelToCount: 10,

    // selectors for get node info
    selectorTitle: '[data-element="ProductBody"] a',
    selectorCount: '[data-element="SelectedOption"]',
    selectorPrice: '[data-element="PriceSection"] span',
    selectorImage: '[data-element="Image"] img'
  })

  setTimeout(() => {
    currys.init()
  }, 5000)

  const handlersOptions = {
    ali: {
      classes: {
        [ali.selectClass]: (e) => {
          ali.selectAliExpressProduct(e.target)
        },
        [ali.countClass]: (e) => {
          ali.updatePurchaseCount(e.target)
        }
      }
    }
  }

  document.addEventListener('click', (event) => {
    Object.keys(handlersOptions.ali.classes).forEach(cls => {
      if (event.target.classList.contains(cls)) {
        setTimeout(() => {
          handlersOptions.ali.classes[cls](event)
          domCreator.renderList(ali.allPurchases, ali.totalSum)
        })
      }
    })
  })
}
