class Currys extends ShopPurchase {
  constructor({
    changeCountSelector,
    productSelector,
    removeSelector,

    parentNodeLevelToCount,
    parentNodeLevelToRemove,

    selectorTitle,
    selectorCount,
    selectorPrice,
    selectorImage
  }) {
    super()

    this.domCreator = new DomCreator()

    this.changeCountSelector = changeCountSelector
    this.productSelector = productSelector
    this.removeSelector = removeSelector

    this.parentNodeLevelToCount = parentNodeLevelToCount
    this.parentNodeLevelToRemove = parentNodeLevelToRemove

    this.selectorTitle = selectorTitle,
    this.selectorCount = selectorCount,
    this.selectorPrice = selectorPrice,
    this.selectorImage = selectorImage

    this.purchases = []
    this.totalPrice = 0
  }

  get allPurchases() {
    return this.purchases
  }

  get totalSum() {
    return this.totalPrice
  }

  #onRemove = (e) => {
    setTimeout(() => {
      this.purchases = []
      this.init()
    }, 2000)
  }

  #onChangeCount = (e) => {
    const count = e.target.innerHTML
    const productNode = super.getNodeFromLevel(e.target, this.parentNodeLevelToCount)
    const productImage = productNode.querySelector(this.selectorImage).src

    this.purchases = this.purchases.map(
      item => item.image === productImage ? { ...item, count } : item
    )
    this.calculatePrice()
    this.domCreator.renderList(this.allPurchases, this.totalSum)
  }

  init() {
    this.getPurchasesList()
    this.calculatePrice()
    this.domCreator.renderList(this.allPurchases, this.totalSum)
  }

  calculatePrice() {
    this.totalPrice = super.calculatePrice(this.purchases)
    console.log(this.totalPrice)
  }

  getPurchasesList() {
    document.querySelectorAll(this.productSelector).forEach(product => {
      //remove
      product.querySelectorAll(this.removeSelector).forEach(removeItem => {
        removeItem.addEventListener('click', this.#onRemove)
      })
      // change count
      product.querySelectorAll(this.changeCountSelector).forEach(countItem => {
        countItem.addEventListener('click', this.#onChangeCount)
      })

      this.purchases.push({
        title: product.querySelector(this.selectorTitle).innerHTML,
        price: product.querySelector(this.selectorPrice).innerHTML,
        count: parseInt(product.querySelector(this.selectorCount).innerHTML),
        image: product.querySelector(this.selectorImage).src,
      })
    })
  }
}
