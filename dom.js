class DomCreator {
  constructor() {
    this.wrapperClass = 'shop-wrapper'
    this.productClass = 'product-item'
    this.totalPriceClass = 'shop__price'
    this.productImageClass = 'product__image'
    this.productTitleClass = 'product__title'
    this.productPriceClass = 'product__price'
    this.productAmountClass = 'product__amount'
  }

  createElem({ tag, cls, text, style }) {
    const elem = document.createElement(tag)
    elem.className = cls
    elem.innerHTML = text || ''
    if (style) {
      elem.style = style
    }
    return elem
  }

  removeList() {
    const parentElem = document.querySelector(`.${this.wrapperClass}`)
    if (parentElem) parentElem.remove()
  }

  createWrapper() {
    const elem = this.createElem({ tag: 'div', cls: this.wrapperClass })
    document.body.appendChild(elem)
    return elem
  }

  renderList(items, sum) {
    this.removeList()
    const wrapper = this.createWrapper()
    const priceElem = this.createElem(
      { tag: 'div', cls: this.totalPriceClass, text: `Total price: ${sum}` }
    )

    wrapper.appendChild(priceElem)

    items.forEach(item => {
      // create elements of product
      const productElem = this.createElem({ tag: 'div', cls: this.productClass })
      const productImageElem = this.createElem(
        { tag: 'div', cls: this.productImageClass, style: `background-image: url(${item.image});` }
      )
      const productTitleElem = this.createElem(
        { tag: 'div', cls: this.productTitleClass, text: item.title }
      )
      const productPriceElem = this.createElem(
        { tag: 'div', cls: this.productPriceClass, text: item.price}
      )
      const productAmountElem = this.createElem(
        { tag: 'div', cls: this.productAmountClass, text: item.count }
      )
      
      // insert to product
      const productsElems = [productTitleElem, productImageElem, productAmountElem, productPriceElem]
      productsElems.forEach(elem => {
        productElem.appendChild(elem)
      })
      wrapper.appendChild(productElem)
    })
  }
}