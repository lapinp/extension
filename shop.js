class ShopPurchase {
  #getPriceValue = priceWithStr => +priceWithStr.replace(/[^\d\.]/g, '')

  calculatePrice(purchases) {
    return purchases.reduce(
      (result, item) => result += this.#getPriceValue(item.price) * item.count, 0
    )
  }

  getNodeFromLevel(node, level) {
    let startNode = node
    for (let i = 0; i < level; i++) {
      startNode = startNode.parentNode
    }
    return startNode
  }
}
